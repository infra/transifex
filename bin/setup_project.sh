#!/bin/bash
# Quick & Dirty wrapper to create a new project or branch to sync
# This has to be run on xfce1 server, in /srv/transifex/repos
# Usage: 
# xfceproject: path of the project: xfce/xfwm4
# txprojectname: project name on transifex side: xfwm4.xfce-4-14
# tag is the branch on xfce side (default to master): xfce-4.14
# Example:
# For an xfce/ project, we might want to checkout a git branch (4.18, 4.20)
# cd repos && ../bin/setup_project.sh xfce/xfce4-panel xfce4-panel.xfce-4-18 xfce-4.18
# For a plugin or a app, we usually only uses master branch
# cd repos && ../bin/setup_project.sh panel-plugins/xfce4-docklike-plugin xfce-panel-plugins.xfce4-docklike-plugin
set -euo pipefail
TX="/srv/transifex/bin/tx"

xfceproject=$1
txprojectname=$2
tag=${3:-master}
project=$(echo $txprojectname | cut -d"." -f1)
resource=$(echo $txprojectname | cut -d'.' -f2)

git clone  https://gitlab.xfce.org/${xfceproject}.git ${txprojectname} -b ${tag}
cd ${txprojectname}
$TX init
# Only support intltool init for now (no native gettext)
cd po
/usr/bin/xgettext --default-domain=${txprojectname} --directory=.. --add-comments=TRANSLATORS: --from-code=UTF-8 --keyword=_ --keyword=N_ --keyword=C_:1c,2 --keyword=NC_:1c,2 --keyword=g_dngettext:2,3 --add-comments --files-from=./POTFILES.in --copyright-holder=Xfce --package-name=${txprojectname} --msgid-bugs-address https://gitlab.xfce.org/ --output=${txprojectname}.pot
sed -i 's/^"Content-Type: text\/plain; charset=CHARSET\\n"$/"Content-Type: text\/plain; charset=UTF-8\\n"/' ${txprojectname}.pot
sed -i "s/PACKAGE VERSION/${project}/" ${txprojectname}.pot
cd ..
# This will configure tx, and push any local languages translated to tx.
# Then tx will add all lang on tx side, and they will become available on ext txcron.sh run (and when transifex memory/translators will translate strings)
$TX add --organization xfce --project ${project} --resource ${resource} --resource-name ${resource} --file-filter "po/<lang>.po" --type PO po/${txprojectname}.pot
$TX push -s -t
