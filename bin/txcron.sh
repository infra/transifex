#!/bin/bash
#
# Copyright (c) 2013 Nick Schermer <nick@xfce.org>
# 		2023 Xfce development team
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307  USA
#

BASE="/srv/transifex/repos/"
NOBODY="Anonymous <noreply@xfce.org>"
MINPERC="50"
UPDATECREDITS=false

# direct commands
GIT="/usr/bin/git"
TX="/srv/transifex/bin/tx"
FIND="/usr/bin/find"
INTLTOOL_UPDATE="/usr/bin/intltool-update"
XGETTEXT="/usr/bin/xgettext"

# refresh log
rm /tmp/txpull.log &> /dev/null

# walk through all the known resources (git clones)
for path in `$FIND $BASE -mindepth 1 -maxdepth 1 -name "*.*" -type d | sort`
#for path in $(echo /srv/transifex/repos/xfce-panel-plugins.xfce4-notes-plugin/)
do
  resource=$(basename "${path}")
  project=$(echo ${resource} | cut -d'.' -f2)
  branchname="${resource//*./}"
  echo $resource
  translation_setup=false

  # go into the repo
  cd "${path}" || exit 1
  # weak detection of the translation system
  if [[ -f "po/Makevars" ]];
  then
    podir=po
    translation_setup=xgettext
  elif [[ -f "po/POTFILES.in" ]]
  then
    podir=po
    translation_setup=intltool
  elif [[ -f "lib/po/POTFILES.in" ]]
  then
    # for www repo
    podir=lib/po
    translation_setup=php
  else
    echo "[${resource}] No POTFILES.in found so could not determn the po directory"
    continue
  fi

  # check if extracted names match
  trackingbranch=$($GIT rev-parse --symbolic-full-name --abbrev-ref @\{u\})
  if [[ ("${branchname}" == "master" || "${branchname}" == "xfce-" ) \
        && "${trackingbranch/./-}" != "origin/${branchname}" ]]
  then
    echo "[${resource}] Branch names to not match (${trackingbranch})!"
  fi

  # cleanup repository
  $GIT clean --quiet -xfd -e '.tx/'
  $GIT reset --quiet --hard ${trackingbranch}

  # pull new changes
  $GIT pull --quiet

  # be sure there are no old transifex files left in the tree
  rm -r "${podir}/"*".po.new" ".tx/${resource}/" 2>/dev/null
  mkdir -p ".tx/${resource}"

  # fetch new translations from the transifex server
  $TX pull --use-git-timestamps --skip -r "${resource}" --minimum-perc="${MINPERC}" -a 1>>/tmp/txpull.log

  $GIT status --porcelain "${podir}/*.po" | while read status f;
  do
    echo $f
    author=${NOBODY}
    lang=`basename ${f%.po}`

    # check the translation
    err=`msgfmt --check -o /dev/null "${f}" 2>&1`
    if [[ "$?" -ne "0" ]]
    then
      echo "Msgfmt failed for ${resource}"
      echo "${err}"
      # Lang is not in git, remove it for current dir to not mess with it
      if [ "$status" = "??" ];
      then
        rm -f "${f}"
      fi
      continue
    fi

    # statistics for in the commit message
    stats=`msgfmt -o /dev/null --statistics "${f}" 2>&1`

    # percentage complete for the commit title
    x=$(echo "${stats}" | sed -e 's/[,\.]//g' \
           -e 's/\([0-9]\+\) translated messages\?/tr=\1/' \
           -e 's/\([0-9]\+\) fuzzy translations\?/fz=\1/' \
           -e 's/\([0-9]\+\) untranslated messages\?/ut=\1/')
    eval "tr=0 fz=0 ut=0 ${x}"
    total=$((${tr} + ${fz} + ${ut}))
    perc=$((100 * ${tr} / ${total}))
    if [ "$status" = "M" ];
    then
      msgtitle="Update"
    fi
    if [ "$status" = "??" ];
    then
      msgtitle="Add new"
      $GIT add "${f}" 1> /dev/null
    fi
    # commit the update
    $GIT commit -m "I18n: ${msgtitle} translation ${lang} (${perc}%)." \
                -m "${stats}" \
                -m "Transifex (https://explore.transifex.com/xfce/)." \
                --author "${author}" --quiet "${f}" 1> /dev/null
    # update credits in database
    if [[ ${UPDATECREDITS} = true ]]
    then
      if [[ -f "$HOME/mysql-password" && "${author}" != "${NOBODY}" ]]
       then
         mysql -u transifex transifex <<EOF
	   INSERT INTO credits(identity,lang_code)
	   VALUES ('${author}','${lang}')
	   ON DUPLICATE KEY UPDATE n_commits=n_commits+1, last_commit=CURRENT_TIMESTAMP;
EOF
      fi
    fi
 
  done

  # We are using pure gettext
  if [ "$translation_setup" = "xgettext" ]
  then
    # generate a new potfile
    pushd "${podir}" 1>/dev/null
    err=$($XGETTEXT --default-domain=${project} --directory=.. --add-comments=TRANSLATORS: --from-code=UTF-8 \
           --keyword=_ --keyword=N_ --keyword=C_:1c,2 --keyword=NC_:1c,2 --keyword=g_dngettext:2,3 --add-comments --files-from=./POTFILES.in \
           --copyright-holder='Xfce' --package-name="${resource}" --msgid-bugs-address https://gitlab.xfce.org/ --output=${resource}.pot)
    # Update LINGUAS
    echo "# Generated by https://gitlab.xfce.org/infra/transifex" > LINGUAS
    for i in *.po; do test -e "$i" && basename -- "$i" .po; done >> LINGUAS
    $GIT commit -m "I18n: Update po/LINGUAS list" --author "${author}" --quiet LINGUAS 1> /dev/null
  # For www
  elif [ "$translation_setup" = "php" ]
  then
    pushd "${podir}" 1>/dev/null
    files=$(find ../../pages -name "*.php" -printf "%P ")
    err=$($XGETTEXT -kR_ -kE_ -L php --package-name www.xfce.org --from-code=UTF-8 \
           --msgid-bugs-address https://gitlab.xfce.org/www/www.xfce.org -o "${resource}".pot -D ../../pages $files 2>&1)
  # We are still using intltool
  elif [ "$translation_setup" = "intltool" ];
  then
    # generate a new potfile
    pushd "${podir}" 1>/dev/null
    err=$($INTLTOOL_UPDATE --pot --gettext-package "${resource}" 2>&1)
  else
    echo "Can't detect the translation system, continue on next project"
    continue
  fi
  # We force the CHARSET to UTF-8 to avoid gettext error on compilation...
  sed -i 's/^"Content-Type: text\/plain; charset=CHARSET\\n"$/"Content-Type: text\/plain; charset=UTF-8\\n"/' ${resource}.pot
  popd 1>/dev/null

  # check if the new file exists
  potfile="${podir}/${resource}.pot"
  if [[ ! -f "${potfile}" ]]
  then
    # generate message
    mailx -s "[Xfce] Pot generation failed for ${resource}" "sysadmin@xfce.org" << EOF
Hi,

This is an automatically generated message from the Xfce Transifex bot.
Please do not reply to this message.

Reported the following issue:

====
${err}
====

Please resolve this issue in the repository.

Sincerely,
Xfce
EOF

    echo "[${resource}] Failed to generate POT file."
    continue
  fi


  err=$($GIT push --quiet 2>&1)
  if [[ "$?" -ne "0" ]]
  then
    echo "[${resource}] git push failed:"
    echo "${err}"
  fi

  # check if the potfile requires an update
  potcache=".tx/${resource}.pot"
  count=`msgcat -u "${potcache}" "${potfile}" 2>&1 | wc -c`
  if [[ "${count}" -gt "0" ]]
  then
    # push the new translation to transifex.com
    $TX push -r "${resource}" --source #  1>/dev/null

    # updated the cached pot file
    cp "${potfile}" "${potcache}"

  fi
done
