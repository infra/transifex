txcron
======

Cronjob script to auto-magically push new/updated [transifex.com](https://transifex.com) translations to git repositories. It uses the tx cli available at https://github.com/transifex/cli/

The script is created for the Xfce Desktop Environment to automate po file updates from translators to the upstream repositories, without intervention of developers. It also checks if there are new pot files needed for the translators, so code and transifex.com are always in-sync.

This part run on xfce1.xfce.org instance

Setup
-----

Setup if fairly easy, it walks a directory tree located in *$BASE* where each folder has the layout *$RESOURCE.$PROJECT*, corresponding with the project and resource name on the Transifex website. 

    $ tree -L 1 repos/ | head -n 15
    repos/
    ├── exo.master
    ├── garcon.master
    ├── garcon.xfce-4-18
    ├── libxfce4ui.master
    ├── libxfce4ui.xfce-4-18
    ├── libxfce4util
    ├── libxfce4util.master
    ├── libxfce4windowing.main
    ├── thunar.master
    ├── thunar-plugins.thunar-archive-plugin
    ├── thunar-plugins.thunar-media-tags-plugin
    ├── thunar-plugins.thunar-shares-plugin
    ├── thunar-plugins.thunar-vcs-plugin
    ├── thunar-volman.master

It assumes the user under which the script is executed has read/write permissions to the git repository, most likely using pubkeys.

You need an API key for transifex API.

What is does
------------
* Reset and pull the tracking branch of the repo.
* Pull new transifex translations.
* Update or add new translations if modified, make nice commit message.
* Regenerate pot file if not found.
* Push new pot file if required.

MySQL (Deprecated, not used anymore)
------------------------------------
For credit purposes it can also maintain a list of authors. For that create a user and database as defined in the .sql file and put the password in *$HOME/mysql-password*. It will then count commits per-user/language and maintain the last timestamp. This can for example be user to add translators credits to the code with a moving window of the last year.
Disabled by default

Add a project or branch
-----------------------

Setuping a new project assume that the project uses native gettext (and not intltool anymore).

A script in `bin/setup_project.sh` can help you to setup a new project to sync ! 

The script does the following steps.

The current setup check master branch of every components. You may need to configure a branch (for example xfce-4.18)

    $ git clone  https://gitlab.xfce.org/xfce/xfce4-panel.git xfce4-panel.xfce-4-18 -b xfce-4.18
    $ cd xfce4-panel.xfce-4-18
    $ tx init
    $ cd po

Create the .pot source with xgettext

    $ /usr/bin/xgettext --default-domain=xfce4-panel.xfce-4-18 --directory=.. --add-comments=TRANSLATORS: --from-code=UTF-8 --keyword=_ --keyword=N_ --keyword=C_:1c,2 --keyword=NC_:1c,2 --keyword=g_dngettext:2,3 --add-comments --files-from=./POTFILES.in --copyright-holder=Xfce --package-name=xfce4-panel.xfce-4-18 --msgid-bugs-address https://gitlab.xfce.org/ --output=xfce4-panel.xfce-4-18.pot

It then edit .pot and change CHARSET to UTF-8 and PACKAGE VERSION to the component name, and configure tx to match all "po/<lang>.po" files.

    cd ..
    $ tx add --organization xfce --project ${project} --resource ${resource} --resource-name ${resource} --file-filter "po/<lang>.po" --type PO po/${txprojectname}.pot

And now we push source and translation to transifex resource. This will also create the project on transifex side!

    $ tx push -s -t

Check the output, some import problems can occurs (mostly language with plural forms (ru, pl) because we reimport old .po, I don't know how to fix this unfortunately, reimporting the one from 'master' branch works most of the time.
